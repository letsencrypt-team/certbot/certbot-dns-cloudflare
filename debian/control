Source: python-certbot-dns-cloudflare
Maintainer: Debian Let's Encrypt <team+letsencrypt@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>,
           Andrew Starr-Bochicchio <asb@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-acme-abi-2 (>= 2.0),
               python3-certbot-abi-2 (>= 2.0),
               python3-cloudflare,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Standards-Version: 4.6.1
Homepage: https://certbot.eff.org/
Vcs-Git: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-cloudflare.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/certbot/certbot-dns-cloudflare
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-certbot-dns-cloudflare
Architecture: all
Depends: certbot, python3-certbot-abi-2 (>= ${Abi-major-minor-version}), ${misc:Depends}, ${python3:Depends}
Enhances: certbot
Description: Cloudflare DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the Cloudflare DNS plugin to the main application.

Package: python-certbot-dns-cloudflare-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Documentation for the Cloudflare DNS plugin for Certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This package contains the documentation for the Cloudflare DNS plugin to
 the main application.
